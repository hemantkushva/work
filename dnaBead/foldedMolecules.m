function  foldedMolecules(imgArr)
r=groot;
S=createMoleculeGraph(imgArr,ones(size(imgArr)));
sizeImgArr=size(imgArr);
outImgArr=imgArr;



f=figure('Position',r.ScreenSize,'Visible','off','Pointer','crosshair','CloseRequestFcn',@closeFigureCallBack);
ax1=axes(f,'Units','Normalized','Visible','off');
ax1.Position=[0.5 0.01 0.48 0.48];
ax2=axes(f,'Units','Normalized','Visible','off');
ax2.Position=[0.5 0.51 0.48 0.48];
linkaxes([ax1,ax2],'xy');
axes(ax2),imshow(imgArr);
axes(ax1),imshow(outImgArr);

t=uitable(f,'ColumnName',{'MoleculeLength'});

pb_1=uicontrol('Style','pushButton','CallBack',@getDataPoints,'String','Get Path','Units','Normalized');
pb_2=uicontrol('Style','pushButton','CallBack',@clearDataPoints,'String','Clear DataPoints','Units','Normalized');
pb_3=uicontrol('Style','pushButton','CallBack',@clearPreviousPath,'String','Clear LastPath','Units','Normalized');
pb_4=uicontrol('Style','pushButton','CallBack',@closeFigureCallBack,'String','Save&Close','Units','Normalized');
pb_1.Position=[0.01 0.7 0.1 0.1];
pb_2.Position=[0.01 0.6 0.1 0.1];
pb_3.Position=[0.01 0.5 0.1 0.1];
pb_4.Position=[0.01 0.8 0.1 0.1];


dcm_obj = datacursormode(f);
set(dcm_obj,'DisplayStyle','datatip','SnapToDataVertex','off','Enable','on');

f.Visible='on';


count=1;
pathCellArr=cell(1,1);





    function getDataPoints(src,event)
        c_info = getCursorInfo(dcm_obj);
        if(length(c_info)~=3)
            warndlg(sprintf('You Must Select Three Points \n Pressing OK will clear old datapoints.'));
            dcm_obj.removeAllDataCursors;
            return;
        end
        pointX=arrayfun(@(k) c_info(k).Position(1),1:3);
        pointY=arrayfun(@(k) c_info(k).Position(2),1:3);
        getCustomisedPath(pointX,pointY);
        count=count+1;
        dcm_obj.removeAllDataCursors;
        drawnow;
    end

    function clearDataPoints(src,event)
        dcm_obj.removeAllDataCursors;
    end

    function clearPreviousPath(src,event)
        if(count>1)
            lengthData=t.Data;
            lengthData(end,:)=[];
            t.Data=lengthData;
            outImgArr(pathCellArr{count-1})=imgArr(pathCellArr{count-1});
            pathCellArr{end}=[];
            count=count-1;
            axes(ax1),imshow(outImgArr);
            drawnow;
        else
            return;
        end
    end
    function closeFigureCallBack(src,event)
        varName = inputdlg('Enter the variable name to save all data in base workspace');
        lengthData=t.Data;
        try
            assignin('base',varName{1},lengthData);
            delete(gcf);
        catch
            delete(gcf);
        end
    end








    function getCustomisedPath(pointX,pointY)
        sourceNode=sub2ind(sizeImgArr,round(pointY(1)),round(pointX(1)));
        middleNode=sub2ind(sizeImgArr,round(pointY(2)),round(pointX(2)));
        terminalNode=sub2ind(sizeImgArr,round(pointY(3)),round(pointX(3)));
        path_1=shortestpath(S,sourceNode,middleNode);
        path_2=shortestpath(S,middleNode,terminalNode);
        path_2=path_2(2:end);%Remove repitition of middleNode
        path=[path_1,path_2];
        [dataY,dataX]=arrayfun(@(nodeNumber) ind2sub(sizeImgArr,nodeNumber),path);
        moleculeLength=sum(sqrt((diff(dataY).^2+diff(dataX).^2)));
        lengthData=t.Data;
        lengthData(end+1,1)=moleculeLength;
        t.Data=lengthData;
        outImgArr(path)=1;
        pathCellArr{count}=path;
        axes(ax1),imshow(outImgArr);
        
    end



end
