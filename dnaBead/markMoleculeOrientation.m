function rgbImgArr=markMoleculeOrientation(imgArr,moleculeInfo)
rows=size(imgArr,1);
cols=size(imgArr,2);
rgbImgArr=repmat(imgArr,1,1,3);
nMolecules=length(moleculeInfo);
[yPointsArrCell,xPointsArrCell]=arrayfun(@(moleculeIndx) getGridPoints(moleculeInfo(moleculeIndx).Centroid(1), moleculeInfo(moleculeIndx).Centroid(2)...
    ,moleculeInfo(moleculeIndx).Orientation,moleculeInfo(moleculeIndx).MajorAxisLength,imgArr), 1:nMolecules,'uniformoutput',false);
nPoints=cellfun(@(yPointsArr) length(yPointsArr),yPointsArrCell);
rgbMat=distinguishable_colors(nMolecules,[1,1,1;0,0,0]);
linearIndxArrCell_r= arrayfun(@(k) sub2ind([rows,cols,3],yPointsArrCell{k},xPointsArrCell{k},1*ones(1,nPoints(k))),1:nMolecules,'uniformoutput',false);
linearIndxArrCell_g= arrayfun(@(k) sub2ind([rows,cols,3],yPointsArrCell{k},xPointsArrCell{k},2*ones(1,nPoints(k))),1:nMolecules,'uniformoutput',false);
linearIndxArrCell_b= arrayfun(@(k) sub2ind([rows,cols,3],yPointsArrCell{k},xPointsArrCell{k},3*ones(1,nPoints(k))),1:nMolecules,'uniformoutput',false);

for i=1:nMolecules
rgbImgArr(linearIndxArrCell_r{i})=rgbMat(i,1);
rgbImgArr(linearIndxArrCell_g{i})=rgbMat(i,2);
rgbImgArr(linearIndxArrCell_b{i})=rgbMat(i,3);
end

end