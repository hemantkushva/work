function [moleculeBoxCell,moleculeBitMapCell,completeBitMask_labelled,extractedMolecules,boundariesMarked] =getMoleculeBoxCell(moleculeInfo,imgArr,maskType)
%{

"moleculeBoxCell"- a cell array containing images of individual molecules.
For finding out which molecule is where in the main image, refer 
to "completeBitMask_labelled".

"extractedMolecules"- an image , containing all the molecules extracted
with the remaining background set to zero.

"completeBitMask-labelled" - a labelled bit map of pixels corresponding to
each molecule.The value of the pixel refers to the index of the molecule in cell arrays. 
This can be used for finding the location of a particular molecule in the
cell arrays.

"boundariesMarked" - all the final boundaries of each molecules are marked
with a green color.

"moleculeBitMapCell" - individual bitmap of all the molecules.

Author: Hemant Kumar


%}



nMolecules=length(moleculeInfo);
completeBitMask=zeros(size(imgArr));
completeBitMask_labelled=zeros(size(imgArr));
boundaryInfo=arrayfun(@(k) moleculeInfo(k).BoundingBox,1:nMolecules,'uniformOutput',false);

%Get the edge points of the bounding box

y1=arrayfun(@(k) ceil(boundaryInfo{k}(2)),1:nMolecules);
y2=arrayfun(@(k) y1(k)+boundaryInfo{k}(4)-1,1:nMolecules);
x1=arrayfun(@(k) ceil(boundaryInfo{k}(1)), 1:nMolecules);
x2=arrayfun(@(k) x1(k)+boundaryInfo{k}(3)-1,1:nMolecules);


moleculeBoxCellArr_unmasked=arrayfun(@(k) imgArr(y1(k):y2(k),x1(k):x2(k)), 1:nMolecules,'uniformOutput',false);
moleculeBoxCell=cell(1,length(moleculeBoxCellArr_unmasked));
moleculeBitMapCell=cell(1,length(moleculeBoxCellArr_unmasked));


for i= 1:nMolecules
    tmp_moleculeBitMap=moleculeInfo(i).(maskType);
    completeBitMask(y1(i):y2(i),x1(i):x2(i))=completeBitMask(y1(i):y2(i),x1(i):x2(i))|tmp_moleculeBitMap;
    
    tmp_map_unlabelled=completeBitMask_labelled(y1(i):y2(i),x1(i):x2(i))==0;
    completeBitMask_labelled(y1(i):y2(i),x1(i):x2(i))=completeBitMask_labelled(y1(i):y2(i),x1(i):x2(i))+i*(tmp_map_unlabelled.*tmp_moleculeBitMap);
    
    moleculeBoxCell{i}=moleculeBoxCellArr_unmasked{i}.*tmp_moleculeBitMap;
    moleculeBitMapCell{i}=tmp_moleculeBitMap;
end

moleculeBoundaryMask=boundarymask(completeBitMask);
moleculeBoundaryMask=bwmorph(moleculeBoundaryMask,'thin');
boundariesMarked=imoverlay(imgArr,moleculeBoundaryMask,'green');
extractedMolecules=imgArr.*completeBitMask;
end






