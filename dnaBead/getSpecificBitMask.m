function getSpecificBitMask(moleculeInfo,imgArr,maskType)
nMolecules=length(moleculeInfo);
boundaryInfo=arrayfun(@(k) moleculeInfo(k).BoundingBox,1:nMolecules,'uniformOutput',false);
y1=ceil(boundaryInfo{k}(2));
y2=y1+boundaryInfo{k}(4)-1;
x1=ceil(boundaryInfo{k}(1));
x2=x1+boundaryInfo{k}(3)-1;
for =1:nMolecules
imgArr(y1:y2,x1:x2)=moleculeInfo(k).ConvexImage.*imgArr(y1:y2,x1:x2);
end


end