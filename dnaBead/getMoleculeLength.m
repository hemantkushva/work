function [moleculeLength,moleculeLength_fitted,shortestPathMarked,polynomialCoefficients]=getMoleculeLength(moleculeImgArr,moleculeBitMap,orientation,varargin)
%{
DESCRIPTION:
This function finds the shortest path in a molecule that ideally
corresponds to the brightest parts of the molecule and then finds the path
along that path by two methods, 
   1: find the piecewise length of the the shortest path.
   2: fit the shortest path to a polynomial of specified degree and then find
      the length of the fitted curve. 
INPUTS:
"moleculeImgArr"- image of the single molecule

"moleculeBitMap"- bit map of pixels corresponding to the molecule in "moleculeImgArr"

"orientation" - 'diagonal' or 'alignedWithAxes'. Choose the second option
when the molecules are either roughly vertical or horizontal. Otherwise
choose the first option. This determines the way the function chooses the
first and the last node of molecule at which the shortest path will begin
and terminate. For "diagonal", the function find the first and the last non-zero pixels
along the rough diagonal of the image. For "alignedWithAxes", it finds the
unction find the first and the last non-zero pixels along the centre of the
rectange.

Author: Hemant Kumar
%}




nrows=size(moleculeImgArr,1);
ncols=size(moleculeImgArr,2);
if(nargin==3||nargin==4)
    if(nargin==3)
        n=2;
    else
        n=varargin{1};
    end
    rowsRange=1:nrows;
    colsRange=1:ncols;
    if (strcmpi(orientation,'diagonal'))
        [nzY,~]=find(moleculeBitMap(:,1));
        if(nzY(1)>(nrows/2))
            bottomLeftRange=arrayfun(@(k)  sub2ind([nrows,ncols],rowsRange(end-k+1),colsRange(k)),1:round(min(nrows,ncols)/2));
            topRightRange=arrayfun(@(k)  sub2ind([nrows,ncols],rowsRange(k),colsRange(end-k+1)),1:round(min(nrows,ncols)/2));
            
            sourceNode=bottomLeftRange(find(moleculeBitMap(bottomLeftRange),1));
            terminalNode=topRightRange(find(moleculeBitMap(topRightRange),1));
            
        elseif(nzY(1)<=(nrows/2))
            topLeftRange=arrayfun(@(k)  sub2ind([nrows,ncols],rowsRange(k),colsRange(k)),1:round(min(nrows,ncols)/2));
            bottomRightRange=arrayfun(@(k)  sub2ind([nrows,ncols],rowsRange(end-k+1),colsRange(end-k+1)),1:round(min(nrows,ncols)/2));
            
            sourceNode=topLeftRange(find(moleculeBitMap(topLeftRange),1));
            terminalNode=bottomRightRange(find(moleculeBitMap(bottomRightRange),1));
        end
    elseif(strcmpi(orientation,'alignedWithAxes'))
     [nzY,nzX]=   find(moleculeBitMap(:,1));
     yWidth=abs(max(nzY)-min(nzY));
     xWidth=abs(max(nzX)-min(nzX));
     if(yWidth>=xWidth)
      % The molecule is vertical, find the middle column and find the first and the last non-zero pixel along that column
      centreNodes=find(moleculeBitMap(:,round(ncols/2)));
      sourceNode=centreNodes(1);
      terminalNode=centreNodes(end);
         
     else
       % The molecule is horizontal, find the middle row and find the first and the last non-zero pixel along that row
      centreNodes=find(moleculeBitMap(round(nrows/2),:));
      sourceNode=centreNodes(1);
      terminalNode=centreNodes(end);
     end

end
moleculeImageGraph=createMoleculeGraph(moleculeImgArr,moleculeBitMap);
try
    path=shortestpath(moleculeImageGraph,sourceNode,terminalNode);
    
    %Calculate the length of the molecule assuming the arc formed by the path
    %is piecewise linear which is a valid assumption given that the sample
    %points are at the highest resolution possible.
    
    %Method 1: Find the arc lenght of the shortest path
    [dataY,dataX]=arrayfun(@(nodeNumber) ind2sub([nrows,ncols],nodeNumber),path);
    moleculeLength=sum(sqrt((diff(dataY).^2+diff(dataX).^2)));
    
    %Method 2:Fitting the path to a parabola
    dataY=max(dataY)-dataY+1;
    polynomialCoefficients=polyfit(dataX,dataY,n);
    dataY_fitted=polyval(polynomialCoefficients,dataX);
    moleculeLength_fitted=sum(sqrt((diff(dataY_fitted).^2+diff(dataX).^2)));
catch
    path=1;
    moleculeLength=0;
    moleculeLength_fitted=0;
    polynomialCoefficients=0;
    
end
shortestPathMarked=moleculeImgArr;
shortestPathMarked(path)=1;
end
