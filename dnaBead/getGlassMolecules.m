function moleculeDetectionResult=getGlassMolecules(imgArr,boundarySizeThreshold,orientation)
%{

Description: This function takes an image of a surface adsorbed DNA and
gives relevent outputs.


INPUTS:
"orientation"- 'diagonal' or 'alignedWithAxes'. Choose the second option
when the molecules are either roughly vertical or horizontal. Otherwise
choose the first option.

boundarySizeThreshold=minimum number of pixels forming the boundary of a
                      molecule. (Usual value=50)

OUTPUTS:
"moleculeBoxCell"- a cell array containing images of individual molecules.
For finding out which molecule is where in the main image, refer 
to "completeBitMask_labelled".

"extractedMolecules"- an image , containing all the molecules extracted
with the remaining background set to zero.

"completeBitMask-labelled" - a labelled bit map of pixels corresponding to
each molecule.The value of the pixel refers to the index of the molecule in cell arrays. 
This can be used for finding the location of a particular molecule in the
cell arrays.

"labelImage_boundaries" - a labelled bit map of pixels corresponding to the
rough boundaries of the molecules. Same indexing as "completeBitMask-labelled"

"boundariesMarked" - all the final boundaries of each molecules are marked
with a green color.

"moleculeLengths"-Calculated lenght of each molecule with different
method. 
 row_1: diagonal of the rectangle.
 row_2: arc length of the shortest path.
 row_3: ac length of the fitted curve

"shortestPathImages" -Image of each molecule with the shortest path marked
with a high pixel value.

"fittingParameters"- the coefficients of the polynomial fitted to the
shortest path in descending order of degree.
                           
Author: Hemant Kumar
%}
imgArr=mat2gray(imgArr);
bw_edge=edge(imgArr,'canny');
bw_edge_cleaned=bwareaopen(bw_edge,boundarySizeThreshold);
labelImage_boundaries=bwlabel(bw_edge_cleaned);


moleculeInfo=regionprops(bw_edge_cleaned,'BoundingBox','ConvexImage');
nMolecules=length(moleculeInfo);
[moleculeBoxCell,moleculeBitMapCell,completeBitMask_labelled,extractedMolecules,boundariesMarked]=getMoleculeBoxCell(moleculeInfo,imgArr,'ConvexImage');
roughMoleculeLengths=arrayfun(@(k) sqrt(size(moleculeBoxCell{k},1)^2+size(moleculeBoxCell{k},2)^2) ,1:nMolecules);
[shortestPathLengths,shortestPathLengths_fitted,shortestPathImages,fittingParameters]=arrayfun(@(k) ...
    getMoleculeLength(moleculeBoxCell{k},moleculeBitMapCell{k},orientation),1:nMolecules,'uniformoutput',false);

%{
Put the lengths measured through different methods in a single cell array,
 in the order 
 row_1: diagonal length of rectangle
 row_2: arc length of the shortest path
 row_3: arc length of the fitted arc.
%}
moleculeLengths=vertcat(roughMoleculeLengths,horzcat(shortestPathLengths{:}),horzcat(shortestPathLengths_fitted{:}));

%Pack all the variables into a single struct. 
moleculeDetectionResult=v2struct(moleculeBoxCell,labelImage_boundaries,completeBitMask_labelled,extractedMolecules,boundariesMarked,moleculeLengths,shortestPathImages,fittingParameters);


multiImgLinked(moleculeDetectionResult.completeBitMask_labelled,moleculeDetectionResult.boundariesMarked,moleculeDetectionResult.extractedMolecules);


end