function tightBoundaryArea=getTightBoundaryArea(boundaryPixelsArr)
boundaryPixels_X=boundaryPixelsArr(:,1);
boundaryPixels_Y=boundaryPixelsArr(:,2);
[~,tightBoundaryArea]=boundary(boundaryPixels_X,boundaryPixels_Y,1);
end