function S =createMoleculeGraph(moleculeImgArr,moleculeBitMap)
%{
Map a molecule to a digraph.Only the pixels that correspond to the molecule
will be mapped. The default connectvity used is 8.

Author: Hemant Kumar
%}

weightImgArr=1./moleculeImgArr;
[rows,cols]=size(moleculeImgArr);
nodeNumbers=find(moleculeBitMap);
[y,x]=find(moleculeBitMap);
moleculeNodeMap=zeros(size(moleculeBitMap));
moleculeNodeMap(nodeNumbers)=nodeNumbers;
%For every node, find the node numbers within it's 8-connectivity neighbour
neighboursCellArr=arrayfun(@(k) (nonzeros(moleculeNodeMap(max(1,y(k)-1):min(y(k)+1,rows),max(x(k)-1,1):min(x(k)+1,cols))))' ,1:length(nodeNumbers),'uniformoutput',false);
%For each neighbour, calcuate the weight of the edge
weightsCellArr=cellfun(@(neighboursArr) weightImgArr(neighboursArr),neighboursCellArr,'uniformoutput',false);
%repeat the parent node, as many times as the number of neighbours available
parentCellArr=arrayfun(@(k) repmat(nodeNumbers(k),1,length(neighboursCellArr{k})),1:length(nodeNumbers),'uniformoutput',false);
%Concatenate all of them in separate single arrays.
sourceNodeArr=horzcat(parentCellArr{:});
terminalNodeArr=horzcat(neighboursCellArr{:});
weightsArr=horzcat(weightsCellArr{:});
S=digraph(sourceNodeArr,terminalNodeArr,weightsArr);
end