function [moleculeBoxCell,intensityArrCell]=perfectlyOpenMolecules(moleculeInfo,indx)
%this function takes the indx for molecules which have a boundary with a
%leakage. 
%now, we will use their corresponding convexImage to extract the
%molecules.

[moleculeBoxCell,completeBitMask,extractedMolecules]=getMoleculeBoxCell(moleculeInfo(indx),imgArr,'ConvexImage');
% intensityArrCell=getMoleculeIntensities(moleculeBoxCell,moleculeInfo(indx));




end
