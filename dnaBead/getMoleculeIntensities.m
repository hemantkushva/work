function intensityArrCell =getMoleculeIntensities(moleculeBoxCell,moleculeInfo)
nMolecules=length(moleculeInfo);
intensityArrCell=arrayfun(@(k) radon(moleculeBoxCell{k},moleculeInfo(k).Orientation),1:nMolecules,'uniformOutput',false); 
end