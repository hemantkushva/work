function result=DNAGlass(imgArr,psfWidth)
%this should work perfectly well when the molecules are separated apart.
moleculeOutlineImgArr=edge(imgArr);
fillingElement=strel('disk',psfWidth);
moleculeFilledImgArr=imdilate(moleculeOutlineImgArr,fillingElement);
moleculeBitMask=bwareaopen(moleculeFilledImgArr,300);
extractedMolecules_imgArr=moleculeBitMask.*imgArr;
connectedComponentInfo=bwconncomp(moleculeBitMask);
moleculeInfo=regionprops(connectedComponentInfo,'Centroid','BoundingBox','Image','MajorAxisLength','MinorAxisLength','Orientation');
moleculeBoxCell=getMoleculeBoxCell(moleculeInfo,imgArr);
moleculeOrientation_imgArr=markMoleculeOrientation(imgArr,moleculeInfo);
intensityArrCell=getMoleculeIntensities(moleculeBoxCell,moleculeInfo);
result.bitMask=moleculeBitMask;
result.extractedMolecules=extractedMolecules_imgArr;
result.individualMolecules=moleculeBoxCell;
result.orientationMarked=moleculeOrientation_imgArr;
result.barcodes=intensityArrCell;
end