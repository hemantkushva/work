function [ ] = StretchMeander( videoName )

if nargin == 0
%     videoName = 'repetitiveRegionRawDataRotated.tif';  
    videoName = 'Result of 21june2011_spombe_55C_No_filters_1_ok.tif';
%     videoName = 'SPombeVideoLong.tif';    
%     videoName = 'SPombeVideo3.tif';  
end


% Parameters
rad = 3;            % How many pixels to the left and right to average 
                    % around the meander.


% Rotate the video appropriately.
[~, ~, grayFrames] = RotateVideo( videoName );
frameCount = size(grayFrames,3);


% Correct for uneven background illumination via top-hat filtering.
se = strel('disk',5,0);
% tophatGrayFrames = zeros(size(grayFrames));
% for k = 1:frameCount
%     tophatGrayFrames(:,:,k) = imtophat(grayFrames(:,:,k), se);
% end
% averagedImg = mean(tophatGrayFrames,3);
averagedImg = mean(grayFrames,3);
averagedImg = imtophat(averagedImg, se);
averagedImg = averagedImg ./ max(averagedImg(:));

% Overlay the meander and return the parametrization.
[x, y] = OverlayMeander( averagedImg );


% Allocate memory for the kymograph.
outputArr = zeros(frameCount, length(x));


% Create the kymograph by overlaying the meander and calculating the
% intensity curve along it for each frame.
for i = 1:frameCount
    outputArr(i,:) = CalcIntensityTrace(grayFrames(:,:,i), x, y, rad);
end


% Display!
figure(), imagesc(outputArr), colormap(gray);
iptsetpref('ImshowBorder','tight');
figure(), imshow(outputArr,[]);

end



%=========================================================================%
function [ trace ] = CalcIntensityTrace(img, x, y, rad)


% This will store the intensity curve for this time frame.
trace = zeros(1,length(x));
len = 2*rad + 1;


% Calculate the intensity for the first point.
xVals = x(1)-rad:x(1)+rad;
yVals = y(1) * ones(size(xVals));
intensityVals = img(sub2ind(size(img),yVals,xVals));
trace(1) = sum(intensityVals) / len;


% close all
% imagesc(img), colormap(gray), hold on


% Now calculate the intensity for the remaining points (except the last).
for i = 2:length(x)-1
    
    % We'll average over "rad" pixels on either side of each meander point
    % in the direction of the line perpendicular to the meander.
    p1 = [x(i-1), y(i-1)];
    p2 = [x(i+1), y(i+1)];
    currPoint = [x(i), y(i)];
    
    
    % Calculate the slope at currPoint using the points in front of, and
    % behind, it.
    slope = CalcPerpSlope(p1, p2);
    
    if isinf(slope)
        
        xVals = currPoint(1)-rad:currPoint(1)+rad;
        yVals = currPoint(2) * ones(size(xVals));        
        
    elseif slope == 0
        
        yVals = currPoint(2)-rad:currPoint(2)+rad;
        xVals = currPoint(1) * ones(size(yVals));
        
    else
        
        xVals = currPoint(1)-rad:currPoint(1)+rad;
        yVals = round(currPoint(2) + slope * (xVals-currPoint(1)));
        
    end
    
    intensityVals = img(sub2ind(size(img),yVals,xVals));
    trace(i) = mean(intensityVals);
    
%     plot(xVals, yVals, 'g-')

end


% Calculate the intensity for the last point on the meander.
xVals = x(end)-rad:x(end)+rad;
yVals = y(end) * ones(size(xVals));
intensityVals = img(sub2ind(size(img),yVals,xVals));
trace(end) = sum(intensityVals) / len;

end


%=========================================================================%
function [ pSlope ] = CalcPerpSlope(p1, p2)

if p2(2) - p1(2) == 0
    pSlope = 0;
else
    pSlope = -1 / (p2(2) - p1(2)) / (p2(1) - p1(1));
end

end
