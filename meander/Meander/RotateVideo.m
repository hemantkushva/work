function [ angle, averagedImg, grayFrames ] = RotateVideo( fname )
%ROTATEVIDEO aligns meander video so that straight lines are vertical.
%   Algorithm:  1) Averages intensities over all frames in the video.
%               2) Runs edge-detection.
%               3) Runs hough transform to find optimal rotation angle.
%   Returns:    theta, the optimal rotation angle
%               averagedImg, the rotated image from step (1) above


% Read the video as a series of images.

if strcmp(fname(end-3:end),'.avi') == 1
    v = imread(fname);
    rgbFrames = read(v);
    
    % Convert to grayscale.
    resH = v.Height;
    resW = v.Width;
    frameCount = v.NumberOfFrames;
    grayFrames = zeros(resH, resW, frameCount);
    for i = 1:frameCount
        grayFrames(:,:,i) = rgbFrames(:,:,:,i);
    end
    
else
    
    info = imfinfo(fname);
    frameCount = numel(info);
    height = info.Height;
    width = info.Width;
    
    grayFrames = zeros(height, width, frameCount);
    
    for k = 1:frameCount
        grayFrames(:,:,k) = imread(fname, k);
    end
     
end



% Average the intensities
averagedImg = sum(grayFrames,3);
averagedImg = averagedImg ./ max(averagedImg(:));


% Edge detection
edgeAveragedImg = edge(averagedImg);

    
% Hough transform.
[H, theta, ~] = hough(edgeAveragedImg,'Theta',-10:0.01:10);


% Find the peak pt in the Hough transform.
peak = houghpeaks(H);


% Optimal angle obtained from Hough peaks
angle = theta(peak(2));


% Rotate averagedImg
averagedImg = imrotate(averagedImg,angle,'bilinear','crop');


% Rotate the original grayscale frames.
for i = 1:frameCount
    grayFrames(:,:,i) = imrotate(grayFrames(:,:,i),angle,'bilinear','crop');
end

end

