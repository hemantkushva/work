function onlyColoredPixelsBinaryMap=getBinaryMapFromColorImg(lithographyImage);
%This function takes a color Image of nano lithography Map and converts it
%into a binary.
%Assumptions about the colorImg: The actual pixels corresponding to the
%nano-channel are colored. The rest pixels corresponding to some text or
%anything else are in grayscale

map_1=lithographyImage(:,:,1);
map_2=lithographyImage(:,:,2);
map_3=lithographyImage(:,:,3);
onlyColoredPixels_tmp=abs(map_2-map_1)+abs(map_2-map_3)+abs(map_3-map_1);
onlyColoredPixels_tmp(onlyColoredPixels_tmp>0)=1;
onlyColoredPixels_tmp=mat2gray(onlyColoredPixels_tmp);

%Processes specific to the meander lithography image.

onlyColoredPixels_tmp=imfill(onlyColoredPixels_tmp); %Fill in some small holes
sel_vertical=strel('line',10,90);
onlyColoredPixels_tmp=imclose(onlyColoredPixels_tmp,sel_vertical); %cover small vertical gap, initially covered by the white arrow  
sel_horizontal=strel('line',3,0);
onlyColoredPixelsBinaryMap=imclose(onlyColoredPixels_tmp,sel_horizontal);
end