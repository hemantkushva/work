function juxtaposeMovie= juxtaposeMovies(varargin)
%This function takes arbitrary number of movies in the form of a N-d matrix
% and gives out a single movie in which all the movies are placed side by
% side. ALL THE MOVIES MUST BE OF THE SAME DIMENSION

%Author: Hemant Kumar 
nMovies=numel(varargin);
[rows,cols,nframes]=size(varargin{1});
padding=zeros(rows,2);
juxtaposeMovie=zeros(rows,cols*nMovies+2*nMovies,nframes);
for i=1:nframes
juxtaposeFrameCell=arrayfun(@(indx)[varargin{indx}(:,:,i),padding],1:nMovies,'uniformoutput',false);
juxtaposeMovie(:,:,i)=horzcat(juxtaposeFrameCell{:});
end
end