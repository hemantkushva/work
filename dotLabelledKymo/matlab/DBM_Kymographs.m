classdef DBM_Kymographs

  %  DBM_Kymographs - functions related to kymograph
  
  % analysis (creating kymographs from movies, kymograph alignment etc)
  %  i.e. functions  related to
  % the menu option "Kymographs" in DNA Barcode Matchmaker. 
  %  Syntax: [output] = DBM_Kymographs.functionname( input )
  %
  % Other class-files required:
  % DBM_Init
  % DBM_Import
  %
  % Matlab Toolboxes required:
  % Bioinformatics Toolbox, Curve Fitting Toolbox, Image Processing Toolbox
  % Statistics Toolbox
  %
  % This class  containing the following (static) functions:
  % { 'setoptions' , 'makekymos' , 'alignkymos' , ...
  %    'maketimetraces' , 'createkymograph', 'calcintensityinfo' , ...
  %    'wpalign' , 'smooth_img_wpalign' , 'laplacian_of_gaussian' , ...
  %   'findsinglefeature' , 'imgstretch'}; 
  
  
methods(Static)     

    % Define all functions below

    % ---------------------------- Main --------------------------------------%
    %=========================================================================%
    function [ ] = setoptions(varargin)

    % SETOPTIONS - Allows the user to set options for the program.
    %
    % Inputs: none
    %
    % Outputs: none
    % 
    % Effect on DBMMainstruct: changes state variables length or width
    %	(the minimum length of molecules to consider, and the width of the
    %	window to average over when creating kymographs).
    %
    % Dependencies (matlab-functions, MAT-files): DBM_Init.verifythresholds, 
    %	DBM_Import.tile_dbm_figureshomewindow
    %
    % By: Charleston Noble
    %


    global DBMMainstruct

    opt = varargin{3};

    switch opt
        case 'len'
            prompt = {'Molecule length threshold:'};
            defaultVal = {num2str(DBMMainstruct.filterLength)};
        case 'width'
            prompt = {'Averaging window width:'};
            defaultVal = {num2str(DBMMainstruct.windowWidth)};
        case 'info'
            prompt = {'Molecule information threshold:'};
            defaultVal = {num2str(DBMMainstruct.filterInfo)};
    end

    dlg_title = 'Set option';
    num_lines = 1;
    answer = inputdlg(prompt,dlg_title,num_lines,defaultVal);

    if isempty(answer)
        return 
    end

    % Parse the inputs.
    inp = str2double(answer{1});

    % Validate.
    if isnan(inp)
        disp('Invalid input.')
        return
    end

    % Set the option.
    switch opt
        case 'len'
            DBMMainstruct.filterLength = inp;
        case 'width'
            if rem(inp,1) ~= 0
                disp('Width must be an integer value.')
                return
            end
            if inp ~= DBMMainstruct.windowWidth
                DBMMainstruct.windowWidth = inp;
                DBM_Import.importmovies();
            end
        case 'info'
            DBMMainstruct.filterInfo = inp;
    end


    % Plot the home window again.
    if DBMMainstruct.filesImported
        DBMMainstruct = DBM_Init.verifythresholds(DBMMainstruct);
        DBMMainstruct = DBM_Import.tile_dbm_figureshomewindow( DBMMainstruct );
    end


    end
    %=========================================================================%
    function [ ] = makekymos(varargin)

    % MAKEKYMOS - Takes MxNxk video cutouts and makes them into kymographs.
    %	Called only by the UI menu.
    %
    % Inputs: none
    %
    % Outputs: none
    % 
    % Effect on DBMMainstruct: adds kymographs to the DBMMainstruct object
    %
    % Dependencies (matlab-functions, MAT-files): DBM_Init.verifythresholds, 
    %	DBM_Kymographs.createkymograph, built-in functions
    %
    % By: Charleston Noble
    %

    global DBMMainstruct

    [ DBMMainstruct ] = DBM_Init.verifythresholds(DBMMainstruct);

    % Verify that molecules have been found to create kymographs for.
    if DBMMainstruct.moleculesFound

        % Create a figure to hold the kymographs.
        plotCount = DBMMainstruct.passFilterCount;
        cols = ceil(sqrt(plotCount));
        rows = ceil(plotCount / cols);

        fig = figure();
        set(fig,'name','Raw Kymographs')
        axesArr = ...
                CMN_HelperFunctions.generate_subplots( ...
                    fig, rows, cols, 0, [0, 0], [0 0] ...
                );

        if length(axesArr) > plotCount
            delete(axesArr(plotCount+1:end));
            axesArr(plotCount+1:end) = []; 
        end

        % Go through each of the files.
        moleculeIdx = 1;
        for f = 1:DBMMainstruct.fileCount

            % Go through each molecule in the file.
            for i = 1:length(DBMMainstruct.fileMoleculeCell{f})

                % Extract the molecule object.
                obj = DBMMainstruct.fileMoleculeCell{f}{i};

                if obj.passesFilters

                    % Create the kymograph.
                    if ~isempty(obj.frames)
                        kymo = DBM_Kymographs.createkymograph(obj.frames);
                    else
                        kymo = obj.kymograph;
                    end


                    % Attach the kymograph to the common object.
                    obj.kymograph = kymo;
                    DBMMainstruct.fileMoleculeCell{f}{i} = obj;

                    % Show the kymograph.
                    axes(axesArr(moleculeIdx));
                    imagesc(kymo), colormap(gray);
                    set(gca,'ytick',[])
                    set(gca,'xtick',[])
                    box('on')
                    moleculeIdx = moleculeIdx + 1;

                end            
            end
        end

        % Remember that we have generated the kymographs.
        DBMMainstruct.kymographsGenerated = true;
    else
        disp('You must find molecules before generating kymographs.')
    end

    end
    %=========================================================================%
    function [ ] = alignkymos(varargin)

    % ALIGNKYMOS - 	Aligns identified kymographs.
    %
    % Inputs: none
    %
    % Outputs: none
    % 
    % Effect on DBMMainstruct: adds aligned kymographs to the DBMMainstruct
    %
    % Dependencies (matlab-functions, MAT-files): wpalign, DBM_Kymographs.calcintensityinfo, 
    %	built-in functions
    %
    % By: Charleston Noble
    %
    global DBMMainstruct

    if DBMMainstruct.kymographsGenerated

        % Create a figure to hold the kymographs.
        plotCount = DBMMainstruct.passFilterCount;
        cols = ceil(sqrt(plotCount));
        rows = ceil(plotCount / cols);

        fig = figure('visible','off');
        set(fig,'name','Aligned Kymographs')
        axesArr = ...
                CMN_HelperFunctions.generate_subplots( ...
                    fig, rows, cols, 0, [0, 0], [0 0] ...
                );

        if length(axesArr) > plotCount
            delete(axesArr(plotCount+1:end));
            axesArr(plotCount+1:end) = []; 
        end

        % Go through each of the files.
        moleculeIdx = 1;
        for f = 1:DBMMainstruct.fileCount

            % Go through each molecule in the file.
            for i = 1:length(DBMMainstruct.fileMoleculeCell{f})
                
                % header from file name
                [~, name, ext] = fileparts(DBMMainstruct.fileCell{f}.fileName);
                fileName = [name, ext];

                % Extract the molecule object.
                obj = DBMMainstruct.fileMoleculeCell{f}{i};

                if obj.passesFilters

                    if isempty(obj.alignedKymograph)
                        disp(strcat('Aligning kymograph for file molecule #', num2str(i), ' in file #', num2str(f), ' (', fileName, ')...'))

                        % Extract the kymograph.
                        kymo = obj.kymograph;

                        % Align the kymograph.
                        kymo = DBM_Kymographs.wpalign(kymo);

                        % Attach the aligned kymograph .
                        obj.alignedKymograph = kymo;

                        % Attach the information content also.
                        obj.information = DBM_Kymographs.calcintensityinfo(kymo);

                        % Attach the object to the common object.
                        DBMMainstruct.fileMoleculeCell{f}{i} = obj;

                    else

                        kymo = obj.alignedKymograph;                    

                    end                


                    % Show the kymograph.
                    axes(axesArr(moleculeIdx));
                    imagesc(kymo), colormap(gray);
                    set(gca,'ytick',[])
                    set(gca,'xtick',[])
                    box('on')
                    moleculeIdx = moleculeIdx + 1;    

                    

                    % if filename contains a space, chop off anything preceding the last space in the filename
                    headertext = textscan(fileName, '%s');
                    headertext = headertext{1}(end);

                    headertext = strcat(headertext, ' (File #', num2str(f), ')', ', File Molecule #', num2str(i));

                    xRange=get(gca,'XLim');
                    xCent=(xRange(2)-xRange(1))/2;
                    yRange=get(gca,'YLim');
                    yCent=(yRange(2)-yRange(1))*.03;

                    text(xCent,yCent,headertext,'Color',[0 0.75 0],'HorizontalAlignment','center','Interpreter','none','FontWeight','bold','BackgroundColor',[0 0 0]);

                    
                end            
            end        
        end

        % Remember that we have aligned the kymographs.
        DBMMainstruct.kymographsAligned = true;

    else
        disp('You must generate kymographs before aligning them.')
    end

    end
    %=========================================================================%
    function [ ] = maketimetraces(varargin)

    % MAKETIMETRACES - 	Takes aligned kymographs and makes 
    % 1D intensity profiles from them.
    %
    % Inputs: none
    %
    % Outputs: none
    % 
    % Effect on DBMMainstruct: adds time traces to DBMMainstruct,
    %                          DBMMainstruct.timeAvKymo
    %
    % Dependencies (matlab-functions, MAT-files): built-in functions
    %
    % By: Charleston Noble, Tobias Ambjörnsson
    %

    global DBMMainstruct

    if DBMMainstruct.kymographsAligned

        % Create a figure to hold the kymographs.
        plotCount = DBMMainstruct.passFilterCount;
        cols = ceil(sqrt(plotCount));
        rows = ceil(plotCount / cols);

        fig = figure('visible','off');
        set(fig,'name','Aligned Kymographs')
        axesArr = ...
                CMN_HelperFunctions.generate_subplots( ...
                    fig, rows, cols, .02, [.02, .02], [.02 .02] ...
                );

        if length(axesArr) > plotCount
            delete(axesArr(plotCount+1:end));
            axesArr(plotCount+1:end) = []; 
        end

        moleculeIdx = 1;
        for f = 1:DBMMainstruct.fileCount

            % Go through each molecule in the file.
            for i = 1:length(DBMMainstruct.fileMoleculeCell{f})

                % Extract the molecule object.
                obj = DBMMainstruct.fileMoleculeCell{f}{i};
                if obj.passesFilters
                    if isempty(obj.alignedKymograph)
                        disp('Align all kymographs before generating time averages.')
                    else
                        kymo = obj.alignedKymograph;                    
                    end    

                    % Show the time-averaged kymograph.  
                    axes(axesArr(moleculeIdx));
                    mVals = mean(kymo,1);
                    sVals = std(kymo,[],1) / sqrt(size(kymo,1));
                    hold on
                    plot(mVals,'k')
                    plot(mVals + sVals, 'r:')
                    plot(mVals - sVals, 'r:')
                    % find and plot edges 
                    [ x1, x2 ] = CMN_HelperFunctions.findsignalregion_otsu(mVals);
                    yp = [min(mVals) max(mVals)];
                    xp = x1*[1 1];
                    plot(xp,yp,'k--');
                    xp = x2*[1 1];
                    plot(xp,yp,'k--');
                    hold off                
                    ylabel('I')
                    xlabel('Position (pixels)')
                    box('on')
                    axis tight
                    moleculeIdx = moleculeIdx + 1;    

                    % Store the time-average to the Main struct 
                    xLower = ceil(x1); xUpper = floor(x2);
                    obj.timeAvKymo = mVals(xLower:xUpper); 
                    DBMMainstruct.fileMoleculeCell{f}{i} = obj;
                    
                end            
            end        
        end
    else
        disp('You must align the kymographs before generating time averages.')
    end

    end
    %=========================================================================%
    % ------------------------- Utilities ------------------------------------%
    %=========================================================================%
    function [ rtnKymo ] = createkymograph( frames )

    % CREATEKYMOGRAPH - creates a kymograph from a movie
    %
    % Inputs: frames 	-movie (MxNx3 array)
    %
    % Outputs: rtnKymo 	-the return kymograph
    % 
    % Effect on DBMMainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): built-in functions
    %
    % By: Charleston Noble
    %

    kymo = zeros(size(frames,3), size(frames,1));

    for i = 1:size(kymo,1)
        kymo(i,:) = mean(frames(:,:,i),2);    
    end

    rtnKymo = kymo;

    end
    %=========================================================================%
    function [ info ] = calcintensityinfo(imgArr)

    % CALCINTENSITYINFO - calculates the information score for an intensity curve
    %	calculated from an aligned kymograph
    %
    % Inputs: imgArr	-the aligned kymograph
    %
    % Outputs: info		-the information score
    % 
    % Effect on DBMMainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): CMN_HelperFunctions.robustextrema, built-in 
    %	functions
    %
    % By: Charleston Noble
    %

    % Average
    curve = mean(imgArr);

    noiseEstCurve = zeros(size(imgArr));

    for i = 1:size(imgArr,2)
        noiseEstCurve(:,i) = imgArr(:,i) - mean(imgArr(:,i));    
    end

    sigma_noise = log(std(noiseEstCurve(:)));% +1;

    % Find the region
    [ x1, x2 ] = CMN_HelperFunctions.findsignalregion(curve);

    % Smooth the curve
    smoothWindow = 5;
    curve = smooth(curve, smoothWindow);

    % Get the extrema
    [ extvals ] = CMN_HelperFunctions.robustextrema(curve(x1:x2), sigma_noise);

    % Get the diffs of the extrema
    diffs = abs(diff(extvals));

    % Calculate information
    probVals = normpdf(log(diffs).^2, 0, log(sigma_noise));
    % probVals = (1/(sqrt(2*pi*sigma_noise)))*exp(-log(diffs).^2/(2*sigma_noise));
    probVals(probVals == 0) = [];
    info = sum(-log(probVals));

    end
    %=========================================================================%
    % ------------------ wpalign utilities -----------------------------------%
    %=========================================================================%
    function [vargout] = wpalign(inpImg, leftX, rightX, depth)

    % WPALIGN - recursively aligns a kymograph (see the WPAlign paper)
    %
    % Inputs:
    %	inpImg	-the unaligned kymograph (the only input to the initial fcn call)
    %	leftX	-the left boundary for alignment
    %	rightX	-the right boundary
    %	depth	-how deep the recursion is
    %
    % Outputs: 
    %	vargout	-the aligned kymograph is output in the last step
    % 
    % Effect on DBMMainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): DBM_Kymographs.wpalign, DBM_Kymographs.findsinglefeature, 
    %	DBM_Kymographs.smooth_img_wpalign, DBM_Kymographs.laplacian_of_gaussian, DBM_Kymographs.imgstretch
    %
    % By: Charleston Noble
    %

    global origImgArr baseImgArr featureCount

    W = 5;
    W_trim = 5;
    barrierVal = 10000;

    if nargin <= 1

        baseImgArr = inpImg;
        depth = 1;
        leftX = 1+W;
        rightX = size(baseImgArr,2) - W;
        smoothWindow = 10;
        origImgArr = baseImgArr;
        baseImgArr = DBM_Kymographs.smooth_img_wpalign(baseImgArr, smoothWindow);

    end    

    if rightX-leftX+1 > 25

        imgArr = baseImgArr(:,leftX:rightX);

        [rows, cols] = size(imgArr);

        K = 3;
        windowSize = K * rows;
        if cols < windowSize

            [alignXVals, bestDist] = DBM_Kymographs.findsinglefeature(imgArr, K, barrierVal);

        else

            windowNum = ceil(cols / windowSize) * 2 - 1;
            nbr = ceil(cols / windowSize);
            delta = ceil(cols / nbr);

            alignXValsArr = zeros(rows,windowNum);
            distArr = zeros(1,windowNum);

            sendImgArr = cell(windowNum,1);
            windowStartArr = 1+((1:windowNum)-1)*round((delta/2));
            windowEndArr = 1+(1:windowNum)*round((delta/2));


            for i = 1:windowNum
                windowStartCol = windowStartArr(i);
                windowEndCol = windowEndArr(i);

                if windowEndCol > cols
                    windowEndCol = cols;
                end
                sendImgArr{i} = imgArr(:,windowStartCol:windowEndCol);
            end

            for i = 1:windowNum

                windowStartCol = windowStartArr(i);

                [alignX, dist] = DBM_Kymographs.findsinglefeature(sendImgArr{i}, K, barrierVal);
                alignX = alignX + windowStartCol;
                alignXValsArr(:,i) = alignX;
                distArr(i) = dist;     
            end

            [distArr, idxs] = sort(distArr);
            alignXValsArr = alignXValsArr(:,idxs);

            alignXVals = alignXValsArr(:,1);
            bestDist = distArr(1);        

        end    

        if bestDist < barrierVal        

            featureCount = featureCount + 1;

            imgArr = DBM_Kymographs.imgstretch(baseImgArr(:,leftX-W:rightX+W), alignXVals+W);
            origImgArr(:,leftX-W:rightX+W) = DBM_Kymographs.imgstretch(origImgArr(:,leftX-W:rightX+W), alignXVals+W);
            baseImgArr(:,leftX-W:rightX+W) = imgArr;

            middleX = round(mean(alignXVals)) + leftX - 1;

            DBM_Kymographs.wpalign(inpImg, leftX, middleX-W_trim, depth + 1);
            DBM_Kymographs.wpalign(inpImg, middleX+W_trim, rightX, depth + 1);

        end


        if depth == 1
            vargout = origImgArr;
        end

    end

    end
    %=========================================================================%
    function [imgArr] = smooth_img_wpalign(imgArr, smoothWindow)

    % SMOOTH_IMG_WPALIGN - smooths imgArr using the windowsize smoothWindow
    %	(see the WPAlign paper for details)
    %
    % Inputs: 
    %	imgArr			-the input image to be smoothed
    %	smoothWindow	-the size of the smoothing window
    %
    % Outputs:
    %	imgArr			-the smoothed image
    % 
    % Effect on DBMMainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): built-in functions
    %
    % By: Charleston Noble
    %

    H = fspecial('gaussian', [smoothWindow, smoothWindow], 2);

    padR = smoothWindow;
    padC = smoothWindow;

    imgArr = padarray(imgArr, [padR, padC], 'replicate');

    imgArr = conv2(imgArr, H, 'same');

    imgArr = imgArr(padR:end-padR-1, padC:end-padC-1);
    

    end
    %=========================================================================%
    function [rtnArr] = laplacian_of_gaussian(imgArr, windowSize, sigma)

    % LAPLACIAN_OF_GAUSSIAN - performs a laplacian of Gaussian filter
    %
    % Inputs: imgArr (the image), windowSize (the size of the filter window)
    %	sigma (the parameter for the LoG filter)
    %
    % Outputs: rtnArr (the filtered image)
    % 
    % Effect on DBMMainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): built-in functions
    %
    % By: Charleston Noble
    %

    padX = round(windowSize(1)/2);
    padY = round(windowSize(2)/2);

    imgArr = padarray(imgArr, [padX padY], 'replicate');

    H = fspecial('LoG', windowSize, sigma);

    rtnArr = conv2(imgArr, H, 'same');

    rtnArr = rtnArr(padX:end-padX-1, padY:end-padY-1);

    end
    %=========================================================================%
    function [alignXVals, bestDist] = findsinglefeature(regArr, K, barrierVal)

    % FINDSINGLEFEATURE - detects a single "feature" in the WPAlign algorithm
    %	(see the WPAlign paper for details)
    %
    % Inputs: regArr (the array to detect the feature in), K (the number of
    %	pixels to the left- or to the right- that the feature can jump in
    %	each step), and barrierVal (the parameter B in the algorithm)
    %
    % Outputs: 
    %	alignXVals	-the position of the best feature
    %	bestDist	-the path length of the feature
    % 
    % Effect on DBMMainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): built-in functions
    %
    % By: Charleston Noble
    %

    rows = size(regArr,1);
    cols = size(regArr,2);

    k = DBM_Kymographs.laplacian_of_gaussian(regArr, [2, 6],2);
%       "k" is a matrix with the same size as I which has large positive values in dark
%        bands and large negative values in light bands

    k(k>0) = k(k>0) ./ max(k(:));  % Rescaling the positive values to (0,1]
    k(k<0) = k(k<0) ./ max(-k(:)); % Rescaling the negative values to [-1,0)

    kUp = k;
    kUp(kUp>0) = 1 - kUp(kUp>0); %kUp: Empasizing the bright features of K
    kUp(kUp<=0) = barrierVal; 

    kDown = -k;
    kDown(kDown>0) = 1 - kDown(kDown>0); %kDown: Emphasizing the dark features of K
    kDown(kDown<=0) = barrierVal;

    distArr = [inf, inf];
    alignXValsArr = zeros(rows,2);


    for arbitraryRepeat = 1:2

        if arbitraryRepeat == 1
            weightImg = kUp;
        else
            weightImg = kDown;
        end

        idxs = zeros(1,K);
        idxs(1) = 1;

        P = K-1;
        M = rows*(cols+P) + 2;    

        paddedImg = zeros(size(weightImg,1), size(weightImg,2)+P);
        paddedImg(:,P/2+1:end-P/2) = weightImg;
        paddedImg(:,[1:P/2, end-P/2+1:end]) = -1;
        paddedImg = paddedImg';
        paddedImg = paddedImg(:);

        % Creating row_G with padding connections
        first_row_G = ones(1,cols);    
        second_row_G = 2:M-1-cols-P;    
        x = [2:P/2+1, (2+cols+P/2):(cols+P+1)];
        xRep = repmat(x, rows-1, 1);
        jumpMat = cumsum((cols + P) * ones(rows-1, P)) - (cols+P);
        removeIdxs = xRep + jumpMat - 1;    
        second_row_G(removeIdxs(:)) = [];    
        second_row_G = second_row_G(cumsum(repmat(idxs,1,length(second_row_G))));    
        third_row_G = M-cols-P/2:M-1-P/2;    
        I = [first_row_G, second_row_G, third_row_G];

        % Creating J vector with padding connections
        first_J = P/2+2:cols+P/2+1;
        x = (2+P + cols) : (cols+P) : (2+(rows-1)*(cols+P));
        xRep = repmat(x, cols, 1);
        jumpMat = cumsum(ones(cols, length(x))) - 1;
        idx = xRep+jumpMat;
        idx = idx(:)';
        idxRep = repmat(idx, K, 1);
        jumpMat = cumsum(ones(K, length(idxRep))) - 1;
        second_J = idxRep + jumpMat;
        second_J = second_J(:)';
        J = [first_J, second_J];
        S = paddedImg(J-1);
        third_J = M * ones(1,cols);
        J = [J third_J];
        S = [S; ones(cols, 1)]';

        removeIdxs = find(S == -1);

        I(removeIdxs) = [];
        J(removeIdxs) = [];
        S(removeIdxs) = [];

        [distP, path, ~] = graphshortestpath(sparse(I, J, S, M, M), 1, M, 'method', 'acyclic');

        distArr(arbitraryRepeat) = distP;
        alignXValsArr(:,arbitraryRepeat) = (mod(path(2:rows+1)-1, cols+P) - P/2)';

    end

    [distSort, bestIdx] = sort(distArr);
    bestDist = distSort(1);
    alignXVals = alignXValsArr(:, bestIdx(1));

    end
    %=========================================================================%
    function [outArr] = imgstretch(origImgArr, stretchArr)

    % IMGSTRETCH - interpolates/stretches an image to align a single feature
    %
    % Inputs: origImgArr (the image to stretch), and stretchArr (the output
    %	alignXVals from the function findsinglefeature)
    %
    % Outputs: outArr (the stretched image)
    % 
    % Effect on DBMMainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): built-in functions
    %
    % By: Charleston Noble
    %

    rows = size(origImgArr,1);
    cols = size(origImgArr,2);

    xMean = mean(stretchArr);

    for i = 1:rows

        x = stretchArr(i);

        leftStretch = xMean / x;
        rghtStretch = (cols - xMean) / (cols - x);

        row = origImgArr(i,:);

        cmpXValDiffs = ones(1,cols);
        cmpXValDiffs(1:x) = leftStretch;
        cmpXValDiffs(x+1:end) = rghtStretch;

        cmpXVals = cumsum(cmpXValDiffs);

        cmpIntensityVals = interp1(cmpXVals, row, 1:cols, 'spline');

        origImgArr(i,:) = cmpIntensityVals;

    end

    outArr = origImgArr;

    end
    %=========================================================================%

   
end


end