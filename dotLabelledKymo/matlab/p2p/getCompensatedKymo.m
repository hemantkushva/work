function compensatedKymo=getCompensatedKymo(rawKymoStruct)

averageSignalCellArr=arrayfun(@(i) mean(rawKymoStruct(i).kymo_dynamicMeanSubtraction,2),1:length(rawKymoStruct),'uniformoutput',false);
for i=1:length(rawKymoStruct)
    averageSignal_raw=averageSignalCellArr{i};
    pp=splinefit(1:1000,averageSignal_raw,30);
    averageSignal_fitted=ppval(pp,1:1000);
    meanVal=mean(averageSignal_fitted);
    for j=1:1000
    kymo_main=rawKymoStruct(i).kymo_dynamicMeanSubtraction;
    kymo_show=rawKymoStruct(i).kymo_noSubtraction;
    correctedKymo(j,:)=(meanVal/averageSignal_fitted(j))*kymo_main(j,:);
    correctedKymo_show(j,:)=(meanVal/averageSignal_fitted(j))*kymo_show(j,:);
    
    end
    compensatedKymo(i).kymo_dynamicMeanSubtraction=correctedKymo;
    compensatedKymo(i).kymo_noSubtraction=mat2gray(correctedKymo_show);
    compensatedKymo(i).averageSignal=averageSignal_raw;
    compensatedKymo(i).averageSignal_fitted=averageSignal_fitted;
    clear correctedKymo correctedKymo_show
    
    
    
end
end
