function filledBarcodeCC(alignedKymo,rawKymo,theoreticalData)
indexArr=nchoosek(1:5,2);
%get CC for comparison with theory
barcodeANDtheory=arrayfun(@(i) displayMaxCCconfiguration(theoreticalData.theoreticalBarocde_completeSequence,alignedKymo(i).filledBarcode,2),1:5);
rawFirstANDtheory=arrayfun(@(k) displayMaxCCconfiguration(theoreticalData.theoreticalBarocde_completeSequence,rawKymo(k).kymo_dynamicMeanSubtraction(1,:),2),1:5);
end