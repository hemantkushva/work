function [] = update_ls_fit(barcode,psfSigmaWidth_px,areaOnePeak,onePeakAreaStd,meanBackgroundNoise,stdBackgroundNoise,confidenceInterval,nPeaksSetByUser)
   
        import ELF.Core.run_least_squares_error_min;
        import ELF.Core.calc_chi_squared_error;
        import ELF.Core.calc_param_errors;


        % Divide the barcode in regions for efficiency

        xIdxs = get_x();
        xStartIdx = xIdxs(1);
        xEndIdx = xIdxs(end);
        
        barcodeRegion = barcode(xIdxs);
        peakThresh = mean(barcode)/2;
        highInt = find(barcodeRegion >= peakThresh);
        lowInt = find(barcodeRegion < peakThresh);
        pixStep = diff(lowInt);
        pos = find(pixStep ~= 1)';  % Depending upon the format in which the the barcode is presented one may need to take a transpose of this, otherwise 
        pos = [pos, pos + 1];      % you encounter error on the following if-else loop.
        pos = sort(pos);
        cuts = lowInt(pos);
        if not(mod(length(cuts), 2) == 0)
            if lowInt(1) > highInt(1)
                cuts = [1, cuts];
            else
                cuts = [cuts, length(xIdxs)];
            end
        end
        ranges = zeros(length(cuts)/2, 2);
        rangeIdx = 1;
        for i=1:2:length(cuts)
            ranges(rangeIdx, :) = [cuts(i) - 1, cuts(i + 1) + 1];
            rangeIdx = rangeIdx + 1;
        end
        ranges = ranges + xStartIdx - 1;
        total_fit = zeros(size(barcode)) + meanBackgroundNoise;
        [N_cuts, ~] = size(ranges);
        fprintf('Finding positions...\n')
        
        
        
        %Parallel specific changes.
        pos_all_cellArr =cell(N_cuts,1);
        areas_all_cellArr = cell(N_cuts,1);
        deltAreAll_cellArr = cell(N_cuts,1);
        deltPosAll_cellArr= cell(N_cuts,1);
        total_fit_cellArr=cell(N_cuts,1);
        numTotalPeaks = 0;
        
        xIdxsCurr_cellArr=arrayfun(@(rangeIdx) ranges(rangeIdx,1):ranges(rangeIdx,2), 1:N_cuts,'uniformoutput',false);
       
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   BEGIN ITERATIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        tic
        for rangeIdx = 1:N_cuts
            xIdxsCurr=xIdxsCurr_cellArr{rangeIdx};
            lenCurr = length(xIdxsCurr);
            xStartIdxCurr=xIdxsCurr(1);
            xEndIdxCurr=xIdxsCurr(end);
            
            
            barcodeRegion = barcode(xIdxsCurr);
            % scale area parameters to be of the same order as the position parameters
            scaleFactor = xStartIdxCurr + round(lenCurr/2);
            areaOnePeakScaled = areaOnePeak / scaleFactor;
            if isnan(areaOnePeakScaled)
                areaOnePeakScaled = 1;
            end
                % --------------------------------------- number of peaks -----------------------------------------------
                % read if the number of peaks has been set by the user
            
            
            if (nPeaksSetByUser > 0) && (nPeaksSetByUser == floor(nPeaksSetByUser))
                  numPeaksToFitVect = nPeaksSetByUser;
            else
                nMax = round(sum(barcodeRegion - meanBackgroundNoise) / (areaOnePeak - confidenceInterval * onePeakAreaStd));
                nMin = round(sum(barcodeRegion - meanBackgroundNoise) / (areaOnePeak + confidenceInterval * onePeakAreaStd));
                numPeaksToFitVect = (nMin:nMax);
            end
            
            % --------------------------------------- start iterations ----------------------------------------------
            probGoodFitRegion = 100;
            tmpPeakPositionsVect=cell(length(numPeaksToFitVect),1);
            tmpPeakAreasVect=cell(length(numPeaksToFitVect),1);
            tmpFit=cell(length(numPeaksToFitVect),1);
            probGoodFitTemp=zeros(length(numPeaksToFitVect),1);
            fitQualityBinary=zeros(length(numPeaksToFitVect),1);
            loopIndxVector=numPeaksToFitVect-nMin+1;
            for loopIndx = loopIndxVector % peaks-loop
                % find the best fit for that number of peaks
                numPeaksToFit=loopIndx+nMin-1;
                bestParams = run_least_squares_error_min(numPeaksToFit, scaleFactor, areaOnePeak, onePeakAreaStd, confidenceInterval, xStartIdxCurr, xEndIdxCurr, barcodeRegion, psfSigmaWidth_px, areaOnePeakScaled, meanBackgroundNoise);
                tmpPeakPositionsVect{loopIndx} = bestParams(1:numPeaksToFit);
                tmpPeakAreasVect{loopIndx} = bestParams(numPeaksToFit + (1:numPeaksToFit));
                tmpFit{loopIndx} = zeros(size(xIdxsCurr));
                for peakNum = 1:numPeaksToFit
                    newpeak = areaOnePeakScaled * tmpPeakAreasVect{loopIndx}(peakNum) * gaussmf(xIdxsCurr, [psfSigmaWidth_px, tmpPeakPositionsVect{loopIndx}(peakNum)]);
                    tmpFit{loopIndx} = tmpFit{loopIndx} + newpeak;
                end
                tmpFit{loopIndx} = tmpFit{loopIndx} / (sqrt(2 * pi) * psfSigmaWidth_px) + meanBackgroundNoise;
                chiSquareTemp = calc_chi_squared_error(barcodeRegion, tmpFit{loopIndx}, stdBackgroundNoise);
                probGoodFitTemp(loopIndx) = chi2cdf(chiSquareTemp,lenCurr-2*numPeaksToFit);
               fitQualityBinary(loopIndx)=probGoodFitTemp(loopIndx) <(0.00005 * numPeaksToFit);
           end % end of peaks-loop
            
            potentialFitsIndx=find(fitQualityBinary);
            if(~isempty(potentialFitsIndx))
               fitIndx=potentialFitsIndx(1);
            else
                fitIndx=length(tmpFit);
            end  % out of all the fits for different number of peaks, find either the first that suits a criteria or the last one  
            
            probGoodFitRegion=probGoodFitTemp(fitIndx);
            LSFitRegion=tmpFit{fitIndx};
            peakPositionsVect = tmpPeakPositionsVect{fitIndx};
            areasVect = tmpPeakAreasVect{fitIndx};
            numPeaksRegion = fitIndx+nMin-1;
            
     

                    
            areasVect = areasVect/scaleFactor;
            warning('off', 'MATLAB:nearlySingularMatrix');
            [deltPosRegion, deltAreasRegion] = calc_param_errors(...
                length(barcodeRegion), ...
                peakPositionsVect, ...
                areasVect, ...
                psfSigmaWidth_px, ...
                xStartIdxCurr, ...
                xEndIdxCurr, ...
                stdBackgroundNoise, ...
                areaOnePeak);
            [peakPositionsVect, index]= sort(peakPositionsVect);
            areasVect = areasVect(index);

            
            pos_all_cellArr{rangeIdx} = peakPositionsVect;
            deltPosAll_cellArr{rangeIdx} = deltPosRegion;
            
            
            
            doNotShowAreaStds = false;
            doNotShowAreas = false;
            if not(doNotShowAreas)
                areas_all_cellArr{rangeIdx} = areasVect;
            else
                areas_all_cellArr{rangeIdx} = blanks(size(pos_all_cellArr{rangeIdx},2));
%                 
            end
            if not(doNotShowAreaStds)
                deltAreAll_cellArr{rangeIdx} =  deltAreasRegion;
            else
                deltAreAll_cellArr{rangeIdx} = blanks(size(pos_all_cellArr{rangeIdx},2));
%                 
            end
            numTotalPeaks = numTotalPeaks + numPeaksRegion;
            
            total_fit_cellArr{rangeIdx} = zeros(length(xIdxsCurr), 1) + LSFitRegion';
        end
        toc
        
       
        %Assemble values obtained from each loop in a single variable
        for j=1:N_cuts
            total_fit(xIdxsCurr_cellArr{j})=total_fit_cellArr{j};
        end
        
        pos_all=horzcat(pos_all_cellArr{:});
        deltPosAll=horzcat(deltPosAll_cellArr{:});
        areas_all=horzcat(areas_all_cellArr{:});
        deltAreAll=horzcat(deltAreAll_cellArr{:});
        
        
        

        resultTable = sprintf('#Peak position (pixels)           Area One Peak        \n');
        charPlusMinus = char(177);
        for peakIdx = 1:numTotalPeaks
            resultTable = sprintf('%s %s %17s %s %s %13s %s %s\n', ...
                resultTable, ...
                num2str(peakIdx, '%d'), ...
                num2str(pos_all(peakIdx), '%5.1f'), ...
                charPlusMinus, ...
                num2str(deltPosAll(peakIdx), 1), ...
                num2str(areas_all(peakIdx), '%3.2f'), ...
                charPlusMinus, ...
                num2str(deltAreAll(peakIdx), 1));
        end
        disp(resultTable)
        if numTotalPeaks < 25
            if numTotalPeaks < 15
                fontSize = 8.5;
            elseif numTotalPeaks < 18
                fontSize = 8;
            elseif numTotalPeaks < 22
                fontSize = 7;
            else
                fontSize = 6;
            end
            set(handles.NPEAKS, 'String', numTotalPeaks);
            set(handles.peaksResultLsqFit, ...
                'FontSize', fontSize, ...
                'String', resultTable, ...
                'Visible', 'on')
            
            
            enableHandleNames = { ...
                'chi2EditLS' ...
            };
            update_handle_props(handles, enableHandleNames, 'Enable', 'on');
        end
        
        rectColorA = [0.4, 0.4, 0.4];
        rectColorB = get_peak_pos_rect_color();
        xIdxs = xStartIdx:xEndIdx;
        totalFitRegion = total_fit(xIdxs);
        barcodeRegion = barcode(xIdxs);
        barcodeLineColor =get_barcode_line_color();
        
        % Plot
        
        hAxisLeastSquares = findobj(get(handles.lsqfAx, 'Children'), 'Type', 'axes');
        if isempty(hAxisLeastSquares)
            return;
        end
        if length(hAxisLeastSquares) > 1
            hAxisLeastSquares = hAxisLeastSquares{1};
        end
        axes(hAxisLeastSquares);
        cla(hAxisLeastSquares, 'reset');
        plot(hAxisLeastSquares, xIdxs, barcodeRegion, ...
            'LineWidth', 1.5, ...
            'Color', barcodeLineColor);
        hold(hAxisLeastSquares, 'on');
        plot(hAxisLeastSquares, xIdxs, totalFitRegion, ...
            'LineWidth', 1.5, ...
            'Color', barcodeLineColor);
        hold(hAxisLeastSquares, 'on');

        plot(hAxisLeastSquares, ...
            0, 0, ...
            'Color', rectColorB)
        plot(hAxisLeastSquares, ...
            0, 0, ...
            'Color', rectColorA);
        
        hold(hAxisLeastSquares, 'on');
        
        
        legend('Barcode', 'Fit', 'Peaks with error<2pix', 'Peaks with error>2pix')
        % Chi2 and enable save
        chiSquareError = calc_chi_squared_error(barcodeRegion, totalFitRegion, stdBackgroundNoise);
        degsOfFreedom = (length(xIdxs)- 2*numTotalPeaks - 1);
        probGoodFit = chi2cdf(chiSquareError, length(xIdxs) - 2*numTotalPeaks);
        
        fprintf('Chi^2 Error:          %g\n', chiSquareError)
        fprintf('Degrees of freedom:   %d\n', degsOfFreedom);
        fprintf('p-value:              %g\n', probGoodFit);
        
        enableHandleNames = { ...
            'resetfitLS', ...
            'chi2EditLS', ...
            'browseTosaveB', ...
            'filenameET', ...
            'peakpositionssaveB', ...
            'peakcurvessaveB', ...
            'savefit', ...
            'nmUnitB', ...
            'pixelUnitB', ...
            'saveresultB', ...
            'saveplot' ...
        };
        update_handle_props(handles, enableHandleNames, 'Enable', 'on');
        
        lsFit = true;
        set_fit_data(lsFit, probGoodFit, pos_all, areas_all, totalFitRegion(:));
        set(handles.chi2EditLS, 'String', probGoodFit);
        set_delt_data(deltPosAll, deltAreAll);
    end