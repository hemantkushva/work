function outputStruct=getAnnotated(imgArr,positionCellArr)

%a=[100 50 100 100];
% b=[100 580 100 100];
mainAnnotated=insertObjectAnnotation(imgArr,'rectangle',positionCellArr{1},'A','TextBoxOpacity',0,'TextColor','green','FontSize',25);
mainAnnotated=insertObjectAnnotation(mainAnnotated,'rectangle',positionCellArr{2},'B','TextBoxOpacity',0,'TextColor','green','FontSize',25);


p1=positionCellArr{1};
p2=positionCellArr{2};
sub1=imgArr(p1(2):p1(2)+p1(4),p1(1):p1(1)+p1(3));
sub2=imgArr(p2(2):p2(2)+p2(4),p2(1):p2(1)+p2(3));
sub1=vertcat(ones(25,size(sub1,2)),sub1);
sub2=vertcat(ones(25,size(sub2,2)),sub2);
sub1=insertText(sub1,[1 1],'A','FontSize',15,'BoxOpacity',0,'TextColor','green');
sub1=insertText(sub2,[1 1],'B','FontSize',15,'BoxOpacity',0,'TextColor','green');

outputStruct=v2struct(mainAnnotated,sub1,sub2);



end