function comparisonPlot(diffArrCell,nameArr,indxArr,figName)
set(groot,'defaultTextFontSize',15);
diffArrAll=horzcat(diffArrCell{:});
diffArrAll=round(diffArrAll,2);
absDiffArrAll=abs(diffArrAll);
relativeMax=max(absDiffArrAll);
figure('Name',figName);
ax=axes();
a1=-0.2;
a2=-0.15;
b1=0.2;
b2=-0.15;
c1=0;
c2=0.15;
for j=1:length(diffArrCell)
    diffArr=diffArrCell{j};
    diffArr=round(diffArr,2);
    if(j==1)
        for i=1:length(indxArr)
            if(diffArr(i)>0)
                plot(indxArr(i,1)+a1,indxArr(i,2)+a2,'<','MarkerSize',40*(abs(diffArr(i))/relativeMax)+8,'Color','green');hold on;
                plot(indxArr(i,2)+a1,indxArr(i,1)+a2,'<','MarkerSize',40*(abs(diffArr(i))/relativeMax)+8,'Color','green');hold on;
                text(indxArr(i,1)+a1,indxArr(i,2)+a2,sprintf('%3.2f',diffArr(i)),'Color','black','HorizontalAlignment','center');
                text(indxArr(i,2)+a1,indxArr(i,1)+a2,sprintf('%3.2f',diffArr(i)),'Color','black','HorizontalAlignment','center');
            elseif(diffArr(i)<0)
                plot(indxArr(i,1)+a1,indxArr(i,2)+a2,'<','MarkerSize',40*(abs(diffArr(i))/relativeMax)+8,'Color','red');hold on;
                plot(indxArr(i,2)+a1,indxArr(i,1)+a2,'<','MarkerSize',40*(abs(diffArr(i))/relativeMax)+8,'Color','red');hold on;
                text(indxArr(i,1)+a1,indxArr(i,2)+a2,sprintf('%3.2f',abs(diffArr(i))),'Color','black','HorizontalAlignment','center');
                text(indxArr(i,2)+a1,indxArr(i,1)+a2,sprintf('%3.2f',abs(diffArr(i))),'Color','black','HorizontalAlignment','center');
            elseif(diffArr(i)==0)
                plot(indxArr(i,1)+a1,indxArr(i,2)+a2,'<','MarkerSize',10,'Color','black');hold on;
                plot(indxArr(i,2)+a1,indxArr(i,1)+a2,'<','MarkerSize',10,'Color','black');hold on;
                text(indxArr(i,1)+a1,indxArr(i,2)+a2,'0.00','Color','black','HorizontalAlignment','center');
                text(indxArr(i,2)+a1,indxArr(i,1)+a2,'0.00','Color','black','HorizontalAlignment','center');
            end
        end
    elseif(j==2)
        for i=1:length(indxArr)
            
            if(diffArr(i)>0)
                plot(indxArr(i,1)+b1,indxArr(i,2)+b2,'>','MarkerSize',40*(abs(diffArr(i))/relativeMax)+8,'Color','green');hold on;
                plot(indxArr(i,2)+b1,indxArr(i,1)+b2,'>','MarkerSize',40*(abs(diffArr(i))/relativeMax)+8,'Color','green');hold on;
                text(indxArr(i,1)+b1,indxArr(i,2)+b2,sprintf('%3.2f',diffArr(i)),'Color','black','HorizontalAlignment','center');
                text(indxArr(i,2)+b1,indxArr(i,1)+b2,sprintf('%3.2f',diffArr(i)),'Color','black','HorizontalAlignment','center');
            elseif(diffArr(i)<0)
                plot(indxArr(i,1)+b1,indxArr(i,2)+b2,'>','MarkerSize',40*(abs(diffArr(i))/relativeMax)+8,'Color','red');hold on;
                plot(indxArr(i,2)+b1,indxArr(i,1)+b2,'>','MarkerSize',40*(abs(diffArr(i))/relativeMax)+8,'Color','red');hold on;
                text(indxArr(i,1)+b1,indxArr(i,2)+b2,sprintf('%3.2f',abs(diffArr(i))),'Color','black','HorizontalAlignment','center');
                text(indxArr(i,2)+b1,indxArr(i,1)+b2,sprintf('%3.2f',abs(diffArr(i))),'Color','black','HorizontalAlignment','center');
            elseif(diffArr(i)==0)
                plot(indxArr(i,1)+b1,indxArr(i,2)+b2,'>','MarkerSize',10,'Color','black');hold on;
                plot(indxArr(i,2)+b1,indxArr(i,1)+b2,'>','MarkerSize',10,'Color','black');hold on;
                text(indxArr(i,1)+b1,indxArr(i,2)+b2,'0.00','Color','black','HorizontalAlignment','center');
                text(indxArr(i,2)+b1,indxArr(i,1)+b2,'0.00','Color','black','HorizontalAlignment','center');
            end
        end
    elseif(j==3)
        for i=1:length(indxArr)
            if(diffArr(i)>0)
                plot(indxArr(i,1)+c1,indxArr(i,2)+c2,'^','MarkerSize',40*(abs(diffArr(i))/relativeMax)+8,'Color','green');hold on;
                plot(indxArr(i,2)+c1,indxArr(i,1)+c2,'^','MarkerSize',40*(abs(diffArr(i))/relativeMax)+8,'Color','green');hold on;
                text(indxArr(i,1)+c1,indxArr(i,2)+c2,sprintf('%3.2f',diffArr(i)),'Color','black','HorizontalAlignment','center');
                text(indxArr(i,2)+c1,indxArr(i,1)+c2,sprintf('%3.2f',diffArr(i)),'Color','black','HorizontalAlignment','center');
            elseif(diffArr(i)<0)
                plot(indxArr(i,1)+c1,indxArr(i,2)+c2,'^','MarkerSize',40*(abs(diffArr(i))/relativeMax)+8,'Color','red');hold on;
                plot(indxArr(i,2)+c1,indxArr(i,1)+c2,'^','MarkerSize',40*(abs(diffArr(i))/relativeMax)+8,'Color','red');hold on;
                text(indxArr(i,1)+c1,indxArr(i,2)+c2,sprintf('%3.2f',abs(diffArr(i))),'Color','black','HorizontalAlignment','center');
                text(indxArr(i,2)+c1,indxArr(i,1)+c2,sprintf('%3.2f',abs(diffArr(i))),'Color','black','HorizontalAlignment','center');
            elseif(diffArr(i)==0)
                plot(indxArr(i,1)+c1,indxArr(i,2)+c2,'^','MarkerSize',10,'Color','black');hold on;
                plot(indxArr(i,2)+c1,indxArr(i,1)+c2,'^','MarkerSize',10,'Color','black');hold on;
                text(indxArr(i,1)+c1,indxArr(i,2)+c2,'0.00','Color','black','HorizontalAlignment','center');
                text(indxArr(i,2)+c1,indxArr(i,1)+c2,'0.00','Color','black','HorizontalAlignment','center');
            end
        end
    end
    
end



% for i=1:length(indxArr)
%
%     if(diffArr(i)>0)
%
%         plot(indxArr(i,1),indxArr(i,2),'^','MarkerSize',50*relativeDiffArr(i)+8,'Color','green');hold on;
%         plot(indxArr(i,2),indxArr(i,1),'^','MarkerSize',50*relativeDiffArr(i)+8,'Color','green');hold on;
%         text(indxArr(i,1),indxArr(i,2),sprintf('%+3.2f',diffArr(i)));
%         text(indxArr(i,2),indxArr(i,1),sprintf('%+3.2f',diffArr(i)));
%     elseif(diffArr(i)<0)
%
%         plot(indxArr(i,1),indxArr(i,2),'v','MarkerSize',50*relativeDiffArr(i)+8,'Color','red');hold on;
%         plot(indxArr(i,2),indxArr(i,1),'v','MarkerSize',50*relativeDiffArr(i)+8,'Color','red');hold on;
%         text(indxArr(i,1),indxArr(i,2),sprintf('%+3.2f',diffArr(i)));
%         text(indxArr(i,2),indxArr(i,1),sprintf('%+3.2f',diffArr(i)));
%     else
%
%         plot(indxArr(i,1),indxArr(i,2),'o','MarkerSize',15,'Color','blue');hold on;
%         plot(indxArr(i,2),indxArr(i,1),'o','MarkerSize',15,'Color','blue');hold on;
%         text(indxArr(i,1),indxArr(i,2),sprintf('%+3.2f',diffArr(i)));
%         text(indxArr(i,2),indxArr(i,1),sprintf('%+3.2f',diffArr(i)));
%     end
% end




ax.XTick=1:5;
ax.YTick=1:5;
ax.XTickLabel=nameArr;
ax.YTickLabel=nameArr;

p1=plot(10,10,'<','MarkerSize',15,'Color','black');
p2=plot(10,10,'>','MarkerSize',15,'Color','black');
p3=plot(10,10,'^','MarkerSize',15,'Color','black');
legend([p1 p2 p3],'Aligned Average Signal;','100^{th} Raw Signal;','1^{st} Raw Signal;','Orientation','Horizontal');

ax.XLim=[0.6 5.6];
ax.YLim=[0 6];
grid on;

