function printData_filledBarcode(alignedKymo,figureName)
for i=1:5
figure('Name',sprintf('%s-%d',figureName,i))
plot(alignedKymo(i).barcode,'DisplayName','Barcode');hold on;
plot(alignedKymo(i).filledBarcode,'DisplayName','FilledBarcode');
legend('show')
correctWhiteSpace();
end
end