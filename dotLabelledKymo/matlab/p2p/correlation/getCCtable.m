function getCCtable(alignedKymo,alignedKymo_intensity,rawKymo,theoreticalData)

theory=theoreticalData.theoreticalBarocde_completeSequence;
rawCC=arrayfun(@(i) displayMaxCCconfiguration(rawKymo(i).kymo_dynamicMeanSubtraction(1,:),theory,2),1:5);
aligned_BarcodeCC=arrayfun(@(i) displayMaxCCconfiguration(alignedKymo(i).barcode,theory,2),1:5);
aligned_filledBarcodeCC=arrayfun(@(i) displayMaxCCconfiguration(alignedKymo(i).filledBarcode,theory,2),1:5);
alignedIntensity_BarcodeCC=arrayfun(@(i) displayMaxCCconfiguration(alignedKymo_intensity(i).barcode,theory,2),1:5);
alignedIntensity_filledBarcodeCC=arrayfun(@(i) displayMaxCCconfiguration(alignedKymo_intensity(i).filledBarcode,theory,2),1:5);
defaultGraphics();
figure('Name','finalCCsummary_type3')
plot(rawCC,'--*','DisplayName','Raw-First Frame');hold on;
plot(aligned_BarcodeCC,'--v','DisplayName','Aligned-Barcode');hold on;
plot(aligned_filledBarcodeCC,'--v','DisplayName','Aligned-FilledBarcode');hold on;
plot(alignedIntensity_BarcodeCC,'--v','DisplayName','Aligned-IntensityContinum-Barcode');hold on;
plot(alignedIntensity_filledBarcodeCC,'--v','DisplayName','Aligned-IntensityContinum-FilledBarcode');hold on;
ax=gca;
ax.XLim=[0.5,5.5];
ax.XTick=1:5;
ax.XTickLabel=arrayfun(@(i) sprintf('kymo-%d',i), 1:5,'UniformOutput',false);
legend('show')
correctWhiteSpace();


end