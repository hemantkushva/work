function val=pearsonCorr(signal_1,signal_2)
%signal_1 and signla_2 must be of the same length
denom=sqrt(sum((signal_1).^2))*sqrt(sum((signal_2).^2));
numer=sum(signal_1.*signal_2);
val=numer/denom;
end