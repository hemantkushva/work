function arrCoeff =slidingCC(signal_1,signal_2,slideMax)
if(slideMax==0)
    arrCoeff=pearsonCorr(signal_1,signal_2);
else
    arrCoeff=arrayfun(@(k)   pearsonCorr(signal_1,signal_2(k:k+length(signal_1)-1)),1:slideMax);
end

end