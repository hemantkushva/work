function out=displayMaxCCconfiguration(signal_1,signal_2,type,varargin)
if(type==1)
    displayNameArr=varargin{1};
    displayNameArr=reshape(displayNameArr,1,2);
    figName=displayNameArr{2};
    signal_2=mat2gray(signal_2-min(signal_2));
    if(length(signal_1)>length(signal_2))
        tmp=signal_1;
        signal_1=signal_2;
        signal_2=tmp;
        displayNameArr=flip(displayNameArr,2);
    end
    signal_1=reshape(signal_1,1,length(signal_1));
    signal_2=reshape(signal_2,1,length(signal_2));
    slideMax=abs(length(signal_1)-length(signal_2));
    
    arrCoeff=slidingCC(signal_1,signal_2,slideMax);
    [maxVal,indx]=max(arrCoeff);
    signal_1_shift=indx-1;
    leftPad=zeros(1,indx-1);
    rightPad=zeros(1,length(signal_2)-(indx+length(signal_1)-1));
    signal_1_padded=[leftPad,signal_1,rightPad];
    
    figure('Visible','on','Name',figName),plot(signal_1_padded,'DisplayName',displayNameArr{1}),hold on, plot(signal_2,'DisplayName',displayNameArr{2});
    ax=gca;
    ax.XLabel.String=sprintf('CC is %d',maxVal);
    legend('show');
    outputStruct=v2struct(maxVal,arrCoeff,signal_1_shift,signal_1_padded,signal_1,signal_2,displayNameArr);
    out=outputStruct;
else
  signal_2=signal_2-min(signal_2);
    signal_1=signal_1-min(signal_1);
% signal_1(signal_1<0)=0;
% signal_2(signal_2<0)=0;
    if(length(signal_1)>length(signal_2))
        tmp=signal_1;
        signal_1=signal_2;
        signal_2=tmp;
    end
    
%     signal_2=mat2gray(signal_2-min(signal_2));
%     signal_1=mat2gray(signal_1-min(signal_1));
    
    
    signal_1=reshape(signal_1,1,length(signal_1));
    signal_2=reshape(signal_2,1,length(signal_2));
    slideMax=abs(length(signal_1)-length(signal_2));
    arrCoeff_1=slidingCC(signal_1,signal_2,slideMax);
    arrCoeff_2=slidingCC(flip(signal_1,2),signal_2,slideMax);
    [maxVal_1,~]=max(arrCoeff_1);
    [maxVal_2,~]=max(arrCoeff_2);
    out=max(maxVal_1,maxVal_2);
end


end