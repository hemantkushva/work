function barcode=getTheoreticalBarcode(DNAsequence,templateSequence,psfSigma_pixels,pixelResolution_nm,bpLength_nm)
%{
This function generates a theoretical barcode for an enzymatically labelled
DNA. The way it does this is by converting the letter sequence into it's
corresponding nm scale and placing gaussians of known standard  deviation
(psfSigma_nm: computed through psfSigma_pixels) at the positions where
there should be a fluorophore.

For each pixel, the function then calculates how much distance in nano
meters it corresponds to and then finds the sum of values of each gaussian for that
particular range and then sums it up again. For an efficient calculation, it considers
only the gaussians under whose effective range of +- 3*standard deviation from the
centre, the query range lies.

INPUTS:

DNAsequence: the base sequence of the DNA.

template sequence: sequence at which a fluorescent molecule binds.

psfSigma_pixels: the standard deviation of the PSF of a fluorophore.
                 In Jonathan et.al supplementary, sigma used for 2d localalization is 1.3 pixels.

pixelResolution_nm: how much lenght(nm) is represented by a single pixel.

bpLength_nm: length of a single base pair in nm : Deafult 0.34 nm.

Author: Hemant Kumar
%}

extraPixelPadding=5; %extra pixels at both the ends of the barcode.
psfSigma_nm=psfSigma_pixels*pixelResolution_nm;
const=1/(psfSigma_nm*(2*pi)^(1/2));
gaussianFunc=@(x,shift) exp(-((x-shift)^2)/(2*psfSigma_nm^2)); %The gaussian function

templateOccurence_bpNumber=findstr(DNAsequence,templateSequence); % find index of template occurence, these are the points at which a gaussian will be centered.
templateOccurence_nm=(templateOccurence_bpNumber-1)*bpLength_nm; % what the index corresponds to in terms of actual length in nm.
templateOccurence_nm_padded=(templateOccurence_bpNumber-1)*bpLength_nm + extraPixelPadding*pixelResolution_nm; 
% Each gaussian has only a certain range in which it's values are
% signifcant enough. Usually it's 3*standardDeviation
effectiveRange_left=templateOccurence_nm_padded-3*psfSigma_nm;
effectiveRange_right=templateOccurence_nm_padded+3*psfSigma_nm;

nPixels=ceil((length(DNAsequence)*bpLength_nm)/pixelResolution_nm) + 2*extraPixelPadding ; %How many pixels does the entire sequence need + 10 more, 5 at both ends
barcode=zeros(1,nPixels);

%for each pixel, equidistant points are sampled from it's corresponding nm_range.
nPoints=100;
samplingPointsArr=arrayfun(@(k) linspace((k-1)*pixelResolution_nm,(k-1)*pixelResolution_nm+pixelResolution_nm,nPoints),1:nPixels,'uniformoutput',false);

for i=1:nPixels
    %find the indexes of gaussians that lie close enough to the pixel.
    samplingPoint_left=samplingPointsArr{i}(1);
    samplingPoint_right=samplingPointsArr{i}(end);
    leftDiff=logical(samplingPoint_left-effectiveRange_left>=0);
    rightDiff=logical(effectiveRange_right-samplingPoint_right>=0);
    closeEnoughGaussianIndx=find(leftDiff&rightDiff);
    if(isempty(closeEnoughGaussianIndx))
        barcode(i)=nan;
        continue;
    end
    %For each sampling point in samplingPointsArr{i}, compute the sum of the
    %values of every gaussian that is close enough. And then sum up all the
    %obtained values.
    summedContributionFromEachGaussian=arrayfun(@(x) sum(arrayfun(@(gaussianIndx) gaussianFunc(x,templateOccurence_nm_padded(gaussianIndx)),closeEnoughGaussianIndx)),...
        samplingPointsArr{i});
    barcode(i)= sum(summedContributionFromEachGaussian);
    
end

nanReplaceValue=nanmin(barcode);
barcode(isnan(barcode))=nanReplaceValue;
barcode=mat2gray(barcode);


end