function [featuresCellArray_connected]= connectGroups(featuresCellArray,sizeArr)
% for every connected component in the graph, find the first and the last
% node and put them in separate arrays, for every element in the
% firstnodeArr find the closest match in the lastnodearr.
%Connect the edges.



%%%% VERSION 1-----A centre of mass aligned kymograph ------- %%%%%%%%

%TODO: For now if there is any row based conflicts, all the corresponding
%features are removed from consideration. THIS NEEDS TO BE CHANGE----LINE 54

nFeatures=length(featuresCellArray);

%get the rows and columns of the top and last nodes of each feature.
[topNodeArr,bottomNodeArr]=arrayfun(@(k) deal(featuresCellArray{k}(1),featuresCellArray{k}(end)),1:nFeatures);
[topNode_rowArr,topNode_colArr]= arrayfun(@(k) ind2sub(sizeArr,topNodeArr(k)),1:nFeatures);
[bottomNode_rowArr,bottomNode_colArr]= arrayfun(@(k) ind2sub(sizeArr,bottomNodeArr(k)),1:nFeatures);


% FIRST STEP: LOOK FOR POTENTIAL PARENTS AND DAUGHTERS ONLY ON THE BASIS OF THEIR ROW NUMBER
%It may seem unreasonable to first sort on the basis of row number rather than directly sorting
%on the basis of columns but given that this code will also be further modified for non-COM aligned
% kymographs, it's a reasonable thing to do.

potentialDaughters_featureIndx_cellArr=arrayfun(@(k) find(topNode_rowArr>bottomNode_rowArr(k)), 1:nFeatures,'UniformOutPut',false);
B=potentialDaughters_featureIndx_cellArr;

% SECOND STEP: FILTER OUT THE ROW BASED CANDIDATES ON THE BASIS OF COLUMN NUMBER%

successfulDaughterCandidates_featureIndx=arrayfun(@(k) B{k}(bottomNode_colArr(k)-3<= topNode_colArr(B{k}) &...
    topNode_colArr(B{k}) <= bottomNode_colArr(k)+3),1:nFeatures, 'uniformOutput', false);

C=successfulDaughterCandidates_featureIndx;

% THIRD STEP: ANY CONFLICT?

%look for any row based clash

indxToCheck=find(arrayfun(@(k) numel(C{k}),1:nFeatures)>1); %Gets all the indx in the cell array that has more than one daughterFeatures
for i=indxToCheck
    daughterFeatureIndx=C{i};
    daughter_topNode_rowArr=topNode_rowArr(daughterFeatureIndx);
    [daughter_topNode_rowArr_sorted,sortOrder_top]=sort(daughter_topNode_rowArr);
    
    
    daughter_bottomNode_rowArr=bottomNode_rowArr(daughterFeatureIndx);
    [daughter_bottomNode_rowArr_sorted,sortOrder_bottom]=sort(daughter_bottomNode_rowArr);
    
    if(sortOrder_top==sortOrder_bottom); %there is no conflict among the daughter features on the basis of rows.
        continue;
    else
        C{i}=[];  %  For since the window that is considerd for looking for the daughter
        % features is very narrow, a row-based conflict is something that can't be
        % resolved by simple manner.
    end
    
end

%FOURTH STEP: Put the connected features in a single group.
% To do this,   make a graph out of the feature indexes and then add
% an edge between corresponding nodes if they are present in the cell array
% 'C'

featureConnectivityGraph=graph();
featureConnectivityGraph=addnode(featureConnectivityGraph,nFeatures); %Adds nFeatures nodes to the graph, without any connection

%Now add edges between the feature indexs that need to be connected

for i=1:nFeatures
    featureConnectivityGraph=addedge(featureConnectivityGraph,i,C{i});
end

connectedFeatures_indx=conncomp(featureConnectivityGraph,'outputform','cell'); %find connected components.

%Put all the corresponding node values in a single place
nFeatures_postConnection=numel(connectedFeatures_indx);


featuresCellArray_connected=arrayfun(@(k) vertcat(featuresCellArray{connectedFeatures_indx{k}}),1:nFeatures_postConnection,'UniformOutPut',false);


end