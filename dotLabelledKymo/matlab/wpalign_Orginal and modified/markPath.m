function pathmarked=markPath(imgArr,colPath,val)
%Given a shortest path in terms of colums, mark that with a known pixel
%value.
[r,~]=size(imgArr);
for i=1:r
    imgArr(i,colPath(i))=val;
    pathmarked=imgArr;
end