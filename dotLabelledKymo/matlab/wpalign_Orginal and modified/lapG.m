 function [kUp,kDown,rtnArr] = lapG(imgArr, windowSize, sigma)
barrierVal=10000;

    % LAPLACIAN_OF_GAUSSIAN - performs a laplacian of Gaussian filter
    %
    % Inputs: imgArr (the image), windowSize (the size of the filter window)
    %	sigma (the parameter for the LoG filter)
    %
    % Outputs: rtnArr (the filtered image)
    % 
    % Effect on DBMMainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): built-in functions
    %
    % By: Charleston Noble
    %

    padX = round(windowSize(1)/2);
    padY = round(windowSize(2)/2);

    imgArr = padarray(imgArr, [padX padY], 'replicate');

    H = fspecial('LoG', windowSize, sigma);

    rtnArr = conv2(imgArr, H, 'same');

    rtnArr = rtnArr(padX:end-padX-1, padY:end-padY-1);
    
    
    
    
        
%       "k" is a matrix with the same size as I which has large positive values in dark
%        bands and large negative values in light bands

    rtnArr(rtnArr>0) = rtnArr(rtnArr>0) ./ max(rtnArr(:));  % Rescaling the positive values to (0,1]
    rtnArr(rtnArr<0) = rtnArr(rtnArr<0) ./ max(-rtnArr(:)); % Rescaling the negative values to [-1,0)

    kUp = rtnArr;
    kUp(kUp>0) = 1 - kUp(kUp>0); %kUp: Empasizing the bright features of K
    kUp(kUp<=0) = barrierVal; 

    kDown = -rtnArr;
    kDown(kDown>0) = 1 - kDown(kDown>0); %kDown: Emphasizing the dark features of K
    kDown(kDown<=0) = barrierVal;

    end