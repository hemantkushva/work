function [S,colPath,dist]=createGraph(imgArr,k)
% This function creates a graph and finds the shortest path just like wpalign,
%  except for the fact that instead of having extra top node and a botton
% two of the image pixels are marked as top and bottom node, in hope to give 
% a feature at a desired location.
%Inputs: imgArr: the section of the image on which to run the wpalign
%              k: no. of adjacent pixels to connect to the right and the left.
%Outputs:      S: the created graph 
%              colPath: the shortest path in terms of columns
%              dist: the calculated valuie of the shortest path.

[r,c]=size(imgArr);

imgArr=imcomplement(imgArr);
n_connections= 3*r*c;
row1=zeros(n_connections,1);
row2=zeros(n_connections,1);
row3=zeros(n_connections,1);

node_first=nodeNumber(1,11,c);
node_last=nodeNumber(1000,11,c);


count=1;
     for i=1:r-1
         for j=1:c
             n=nodeNumber(i,j,c);
             if(j<=k||c-j<=k-1)
                [extra,indx]=min([j-1,c-j]);
                addrow1=repmat(n,k+1+extra,1);
                row1(count:count+k+extra,1)=addrow1;
                
                    if(indx-1)
                        addrow2=nodeNumber(i+1,(j-k:j+extra),c);
                        row3(count:count+k+extra)=imgArr(i+1,(j-k:j+extra));
                    else
                        addrow2=nodeNumber(i+1,(j-extra:j+k),c);
                        row3(count:count+k+extra)=imgArr(i+1,(j-extra:j+k));
                    end
                    row2(count:count+k+extra)=addrow2;
                count=(count+k+extra)+1;
             else
                 addrow1=repmat(n,2*k+1,1);
                 row1(count:count+2*k,1)=addrow1;
                 
                 addrow2=nodeNumber(i+1,(j-k:j+k),c);
                 row2(count:count+2*k,1)=addrow2;
                 
                 row3(count:count+2*k,1)=imgArr(i+1,(j-k:j+k));
                 count=(count+2*k)+1;
             end
         end
     end
  
     
     [a,~]=size(row1);
     
     S=sparse(row1,row2,row3,a,a,a);
     [dist,path,~]=graphshortestpath(S,node_first,node_last,'method','Acyclic');
     [~,colPath]=findPixel(path,c);
     
     
end

function n=nodeNumber(i,j,c) %#ok<DEFNU>
n= j+ (i-1)*c;
end