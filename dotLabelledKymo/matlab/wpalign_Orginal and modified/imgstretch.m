function [outArr] = imgstretch(origImgArr, stretchArr)

    % IMGSTRETCH - interpolates/stretches an image to align a single feature
    %
    % Inputs: origImgArr (the image to stretch), and stretchArr (the output
    %	alignXVals from the function findsinglefeature)
    %
    % Outputs: outArr (the stretched image)
    % 
    % Effect on DBMMainstruct: none
   
    % By: Charleston Noble
    %

    rows = size(origImgArr,1);
    cols = size(origImgArr,2);

    xMean = mean(stretchArr);

    for i = 1:rows

        x = stretchArr(i);

        leftStretch = xMean / x;
        rghtStretch = (cols - xMean) / (cols - x);

        row = origImgArr(i,:);

        cmpXValDiffs = ones(1,cols);
        cmpXValDiffs(1:x) = leftStretch;
        cmpXValDiffs(x+1:end) = rghtStretch;

        cmpXVals = cumsum(cmpXValDiffs);

        cmpIntensityVals = interp1(cmpXVals, row, 1:cols, 'spline');

        origImgArr(i,:) = cmpIntensityVals;

    end

    outArr = origImgArr;

    end