function [S,colPath,dist]=createGraphOldStyle(imgArr,k)
%This function implements the wpalign method just the way it was originally
%proposed. Every pixel is connected to the pixel below it and k pixel to
%the left and the right. And then a shortest path is found between the top
%node and bottom node. 
%Inputs: imgArr: the section of the image on which to run the wpalign
%              k: no. of adjacent pixels to connect to the right and the left.
%Outputs:      S: the created graph 
%              colPath: the shortest path in terms of columns
%              dist: the calculated valuie of the shortest path.

[r,c]=size(imgArr);
imgArr=imcomplement(imgArr);
n_connections= r*c;
row1=zeros(n_connections,1);
row2=zeros(n_connections,1);
row3=zeros(n_connections,1);

node_first=1;
node_last=nodeNumber(r,c,c)+1;
row1(1:c,1)=node_first;
row2(1:c,1)=nodeNumber(1,1:c,c);
row3(1:c,1)=1;
count=c+1;
     for i=1:r-1
         for j=1:c
             n=nodeNumber(i,j,c);
             if(j<=k||c-j<=k-1)
                [extra,indx]=min([j-1,c-j]);
                addrow1=repmat(n,k+1+extra,1);
                row1(count:count+k+extra,1)=addrow1;
                
                    if(indx-1)
                        addrow2=nodeNumber(i+1,(j-k:j+extra),c);
                        row3(count:count+k+extra)=imgArr(i+1,(j-k:j+extra));
                    else
                        addrow2=nodeNumber(i+1,(j-extra:j+k),c);
                        row3(count:count+k+extra)=imgArr(i+1,(j-extra:j+k));
                    end
                    row2(count:count+k+extra)=addrow2;
                count=(count+k+extra)+1;
             else
                 addrow1=repmat(n,2*k+1,1);
                 row1(count:count+2*k,1)=addrow1;
                 
                 addrow2=nodeNumber(i+1,(j-k:j+k),c);
                 row2(count:count+2*k,1)=addrow2;
                 
                 row3(count:count+2*k,1)=imgArr(i+1,(j-k:j+k));
                 count=(count+2*k)+1;
             end
         end
     end
     row1(count:count+c-1)=nodeNumber(r,1:c,c);
     row2(count:count+c-1)=node_last;
     row3(count:count+c-1)=1;
     
     [a,~]=size(row1);
     
     S=sparse(row1,row2,row3,a,a,a);
     [dist,path,~]=graphshortestpath(S,node_first,node_last,'method','Acyclic');
     [~,colPath]=findPixel(path(2:end-1)-1,c);
     
     
end

function n=nodeNumber(i,j,c) %#ok<DEFNU>
n= 1 +j+ (i-1)*c;
end