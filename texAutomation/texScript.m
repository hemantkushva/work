function texScript()
fID=fopen('nanoScript.txt','w');
for i=1:14
    
    beginfigure();
    centering();
    beginsubfigure(0.3)
    includeGraphics(sprintf('./fig_bioNano/raw_%d',i));
    caption('Original Kymograph')
    endsubfigure();
    beginsubfigure(0.3)
    includeGraphics(sprintf('./fig_bioNano/rawCOMaligned_%d',i));
    caption('Preliminary Alignment')
    endsubfigure();
    beginsubfigure(0.3)
    includeGraphics(sprintf('./fig_bioNano/rawCOM_local_aligned_%d',i));
    caption('Final Alignment')
    endsubfigure();
    beginsubfigure(1);
    includeGraphics(sprintf('./fig_bioNano/Barcode_%d',i));
    caption(sprintf('Time Averaged Barcode'))
    endsubfigure();
    caption(sprintf('Kymograph-%d Results',i))
    endfigure();
    
end
fclose(fID);

    function newLine()
        fprintf(fID,'\n');
    end
    function beginsubfigure(widthFraction)
        if(widthFraction<1)
            fprintf(fID,'%s','\begin{subfigure}[t]{');
            fprintf(fID,num2str(widthFraction));
            fprintf(fID,'%s','\linewidth}');
            newLine;
        else
            fprintf(fID,'%s','\begin{subfigure}{');
            fprintf(fID,num2str(widthFraction));
            fprintf(fID,'%s','\linewidth}');
            newLine;
        end
    end
    function endsubfigure()
        fprintf(fID,'%s','\end{subfigure}');
        newLine;
    end
    function beginfigure()
        fprintf(fID,'%s','\begin{figure}[H]');
        newLine;
    end
    function endfigure()
        fprintf(fID,'%s','\end{figure}');
        newLine;
    end
    function centering()
        fprintf(fID,'%s','\centering');
        newLine;
    end
    function includeGraphics(fileName)
        fprintf(fID,'%s','\includegraphics[width=0.9\linewidth]{');
        fprintf(fID,fileName);
        fprintf(fID,'}');
        newLine;
    end
    function caption(captionName)
        fprintf(fID,'%s','\captionsetup{justification=centering}');
        newLine;
        fprintf(fID,'%s','\caption{');
        fprintf(fID,captionName);
        fprintf(fID,'}');
        newLine;
    end
end



 






